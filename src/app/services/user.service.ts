import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  apiUrl = environment.apiUrl;
  contentType = { 'Content-Type': 'application/json' };
  api = {
    product: this.apiUrl + '/item',
  };
  token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
  authHeader = {
    headers: {
      Authorization: 'Bearer ' + this.token,
    },
  };
  name: any;
  nameChange: Subject<string> = new Subject<string>();
  private products;

  constructor(private http: HttpClient, private titleService: Title) {
    this.name = 'Jack';
  }

  set productsList(l) {
    this.products = l;
  }

  get productsList() {
    const tmp = this.products;
    // this.products = undefined;
    return tmp;
  }

  setTitle(title) {
    this.titleService.setTitle(title);
  }

  getUserDeliveryDetails() {
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
    return new Promise((resolve, reject) => {
      const endpointUrl = this.apiUrl + '/delivery/contacts';
      this.http
        .get(endpointUrl, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  insertUserDeliveryDetails(data) {
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
    return new Promise((resolve, reject) => {
      const endpointUrl = this.apiUrl + '/delivery/contact';
      this.http
        .post(endpointUrl, data, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  updateUserDeliveryDetails(data) {
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
    return new Promise((resolve, reject) => {
      const endpointUrl = this.apiUrl + '/delivery/contact';
      this.http
        .patch(endpointUrl, data, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  change() {
    this.name = 'Jane';
    this.nameChange.next(this.name);
  }

  setLocalFilterDefaults(defaults) {
    localStorage.filter = JSON.stringify(defaults);
  }

  getProductByName(name) {
    const endpointUrl = this.api.product + `/product/${name}`;

    return new Promise((resolve, reject) => {
      this.http.get(endpointUrl).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getProducts() {
    const endpointUrl = this.api.product + '/products';

    return new Promise((resolve, reject) => {
      this.http.get(endpointUrl).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  insertOrder(order) {
    const endpointUrl = this.apiUrl + '/order/delivery';
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
    return new Promise((resolve, reject) => {
      this.http
        .post(endpointUrl, order, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  getOrders() {
    const endpointUrl = this.apiUrl + '/order/deliveries/logged/user';
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;

    return new Promise((resolve, reject) => {
      this.http
        .get(endpointUrl, {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  cancelOrder(order) {
    const endpointUrl = this.apiUrl + '/order/delivery';
    const token = localStorage.fb ? JSON.parse(localStorage.fb).user : null;
    return new Promise((resolve, reject) => {
      this.http
        .patch(endpointUrl, order, {
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json',
          },
        })
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }
}
