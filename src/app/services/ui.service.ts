import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UiService {
  isLoading = false;

  private subject = new Subject<any>();

  constructor(
    public loadingController: LoadingController,
    public toastController: ToastController
  ) {}

  setCartTotalItems() {
    this.subject.next();
  }

  getClickEvent(): Observable<any> {
    return this.subject.asObservable();
  }

  dismissRefreshContent(event) {
    if (event) {
      event.target.complete();
    }
  }

  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController
      .create({
        spinner: 'crescent',
        message: 'Please wait...',
        translucent: true,
      })
      .then((a) => {
        a.present().then(() => {
          if (!this.isLoading) {
            a.dismiss(null, undefined);
          }
        });
      });
  }

  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingController.dismiss();
  }

  async presentToast(
    message = 'Something went wrong',
    color = 'primary',
    position: any = 'top',
    duration = 500
  ) {
    const toast = await this.toastController.create({
      message,
      color,
      cssClass: 'boldCentered',
      position,
      duration,
    });
    return await toast.present();
  }
}
