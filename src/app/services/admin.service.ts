import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  apiUrl = environment.apiUrl;
  contentType = { 'Content-Type': 'application/json' };
  authorization = { Authorization: 'Bearer ' + localStorage.a };
  authHeader = {
    headers: {
      Authorization: 'Bearer ' + localStorage.a,
    },
  };
  data = {
    transaction: null,
    product: null,
    user: null,
    order: null,
  };

  constructor(private http: HttpClient) {}

  setOrderData(order) {
    this.data.order = order;
  }

  insertOrder(data) {
    const endpoint = this.apiUrl + '/order/delivery';
    return new Promise((resolve, reject) => {
      this.http.post(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getUserOrders(userId) {
    const endpoint = this.apiUrl + '/order/deliveries/user';
    return new Promise((resolve, reject) => {
      this.http.get(endpoint + `/${userId}`, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getStatusOrders(status) {
    const endpoint = this.apiUrl + '/order/deliveries/status';
    return new Promise((resolve, reject) => {
      this.http.get(endpoint + `/${status}`, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateOrder(data) {
    const endpoint = this.apiUrl + '/order/delivery';
    return new Promise((resolve, reject) => {
      this.http.patch(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  deleteOrder(id) {
    const endpoint = this.apiUrl + '/order/delivery/' + id;
    return new Promise((resolve, reject) => {
      this.http.delete(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  setUserData(user) {
    this.data.user = user;
  }

  insertUser(data) {
    const endpoint = this.apiUrl + '/person/user';
    return new Promise((resolve, reject) => {
      this.http.post(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getUsers() {
    const endpoint = this.apiUrl + '/person/users';
    return new Promise((resolve, reject) => {
      this.http.get(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateUser(data) {
    const endpoint = this.apiUrl + '/person/user';
    return new Promise((resolve, reject) => {
      this.http.patch(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  deleteUser(id) {
    const endpoint = this.apiUrl + '/person/user/' + id;
    return new Promise((resolve, reject) => {
      this.http.delete(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  uploadImage(image: File, imageName) {
    const endpoint = this.apiUrl + '/upload/image';
    const formData = new FormData();
    formData.append('image', image, imageName);
    return new Promise((resolve, reject) => {
      this.http.post(endpoint, formData).subscribe(
        (response) => resolve(response),
        (error) => {
          reject(error);
        }
      );
    });
  }

  setProductData(product) {
    this.data.product = product;
  }

  insertProduct(data) {
    const endpoint = this.apiUrl + '/item/product';
    return new Promise((resolve, reject) => {
      this.http.post(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getProducts() {
    const endpoint = this.apiUrl + '/item/products';
    return new Promise((resolve, reject) => {
      this.http.get(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateProduct(data) {
    const endpoint = this.apiUrl + '/item/product';
    return new Promise((resolve, reject) => {
      this.http.patch(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  deleteProduct(id) {
    const endpoint = this.apiUrl + '/item/product/' + id;
    return new Promise((resolve, reject) => {
      this.http.delete(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  setTransactionData(transaction) {
    this.data.transaction = transaction;
  }

  insertTransaction(data) {
    const endpoint = this.apiUrl + '/ledger/transaction';
    return new Promise((resolve, reject) => {
      this.http.post(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getTransactions() {
    const endpoint = this.apiUrl + '/ledger/transactions';
    return new Promise((resolve, reject) => {
      this.http.get(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateTransaction(data) {
    const endpoint = this.apiUrl + '/ledger/transaction';
    return new Promise((resolve, reject) => {
      this.http.patch(endpoint, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  deleteTransaction(id) {
    const endpoint = this.apiUrl + '/ledger/transaction/' + id;
    return new Promise((resolve, reject) => {
      this.http.delete(endpoint, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  getUserContact(userId) {
    return new Promise((resolve, reject) => {
      const endpointUrl = this.apiUrl + '/delivery/contacts/user';
      this.http.get(endpointUrl + `/${userId}`, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  insertUserContact(data) {
    return new Promise((resolve, reject) => {
      const endpointUrl =
        this.apiUrl + '/delivery/contact/user/' + data.contact.userId;
      this.http.post(endpointUrl, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  updateUserContact(data) {
    return new Promise((resolve, reject) => {
      const endpointUrl = this.apiUrl + '/delivery/contact';
      this.http.patch(endpointUrl, data, this.authHeader).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }
}
