import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

@Injectable({
  providedIn: 'root',
})
export class AuthenticateService {
  apiUrl = environment.apiUrl;
  contentType = { 'Content-Type': 'application/json' };
  private messageSource = new BehaviorSubject('Page not found');
  errorMessage = this.messageSource.asObservable();

  constructor(
    private http: HttpClient,
    private router: Router,
    private socialAuthService: SocialAuthService
  ) {}

  fbLogUserIn() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
    return new Promise((resolve, reject) => {
      this.socialAuthService.authState.subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  fbLogUserInDatabase(data) {
    const fullApiUrl = this.apiUrl + '/auth/loginfb';
    return new Promise((resolve, reject) => {
      this.http.post(fullApiUrl, data).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  fbLogUserOut() {
    return this.socialAuthService.signOut();
  }

  logUserIn(data) {
    const fullApiUrl = this.apiUrl + '/auth/login';
    return new Promise((resolve, reject) => {
      this.http.post(fullApiUrl, data).subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  checkUserFbToken() {
    return localStorage.fb ? true : false;
  }

  checkUserToken() {
    return localStorage.a ? true : false;
  }

  setFbApiUrl(authToken) {
    return `https://graph.facebook.com/debug_token?input_token=${authToken}
    &access_token=${environment.fBAppId}|${environment.fBAppSecret}`;
  }

  verifyFbToken() {
    if (!localStorage.fb) {
      return false;
    }
    const token = JSON.parse(localStorage.fb).auth;
    const url = this.setFbApiUrl(token);

    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => reject(error)
      );
    });
  }

  verifyToken() {
    if (!localStorage.a && !localStorage.fb) {
      localStorage.removeItem('fb');
      this.router.navigateByUrl('login');
      throw Error('Cannot verify');
    }
    const fullApiUrl = this.apiUrl + '/token/verify';
    const token = localStorage.a || JSON.parse(localStorage.fb).user;

    const options = {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    };
    return new Promise((resolve, reject) => {
      this.http
        .post(
          fullApiUrl,
          {
            user: 'verify',
          },
          options
        )
        .subscribe(
          (response) => resolve(response),
          (error) => reject(error)
        );
    });
  }

  changeErrorMessage(message: string) {
    this.messageSource.next(message);
  }

  displayErrorPage() {
    this.router.navigateByUrl('/error');
  }
}
