import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { UserService } from 'src/app/services/user.service';
import { Router, NavigationExtras } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';
import { AlertController } from '@ionic/angular';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  quantity: any = '1 pc';
  deliveries;
  show = false;

  constructor(
    private authService: AuthenticateService,
    private userService: UserService,
    private router: Router,
    private uiService: UiService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  async cancelOrder(delivery) {
    const { deliveryId, datetime } = delivery;
    const data = {
      delivery: {
        status: 'Canceled',
        deliveryId,
      },
    };
    try {
      const response: any = await this.userService.cancelOrder(data);
      if (response.data) {
        this.uiService.presentToast('Canceled order');
        this.ionViewWillEnter();
      }
    } catch (error) {
      this.uiService.presentToast('Cannot cancel anymore', 'warning');
    }
  }

  async openConfirmAlert(delivery) {
    const date = new DatePipe('en-US').transform(delivery.datetime, 'medium');
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cancel Order',
      message: `Are you sure you want to cancel your order on ${date}?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Yes',
          handler: () => {
            this.cancelOrder(delivery);
          },
        },
      ],
    });

    await alert.present();
  }

  async ionViewWillEnter() {
    this.deliveries = null;
    try {
      const verifyToken = await this.authService.verifyToken();
      this.getOrders();
      this.show = true;
    } catch (error) {
      this.show = true;
      this.uiService.presentToast('Cannot verify, login again', 'danger');
      localStorage.removeItem('fb');
      this.router.navigateByUrl('login');
    }
  }

  async openProductInfo(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product,
      },
    };
    const name = product.name.split(' ').join('-').toLowerCase();
    this.router.navigate([`product/${name}`], navigationExtras);
  }

  async getOrders(event = null) {
    this.uiService.presentLoading();
    try {
      const response: any = await this.userService.getOrders();
      this.deliveries = response.data;
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
    } catch (error) {
      this.uiService.presentToast('Cannot get orders', 'danger');
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
    }
  }
}
