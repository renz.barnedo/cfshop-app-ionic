import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  devFacebookUrl = 'https://web.facebook.com/barnedorenz/';
  socials = [
    { url: 'https://twitter.com/cafecruzine', icon: 'logo-twitter' },
    { url: 'https://www.instagram.com/cafecruzine', icon: 'logo-instagram' },
    { url: 'https://www.facebook.com/cafecruzine', icon: 'logo-facebook' },
  ];
  constructor(private popoverController: PopoverController) {}

  ngOnInit() {}

  openUrl(url) {
    window.open(url, '_blank');
  }
}
