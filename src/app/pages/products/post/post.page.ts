import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AdminService } from 'src/app/services/admin.service';
import { UiService } from 'src/app/services/ui.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {
  settings = {
    fields: [
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'Name',
        formControlName: 'name',
        dropdown: [],
        position: 'floating',
        placeholder: 'the product/service you purchased',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: false,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'Label',
        formControlName: 'secondName',
        dropdown: [],
        position: 'floating',
        placeholder: 'the product/service you purchased',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'number',
        label: 'Price',
        formControlName: 'price',
        dropdown: [],
        position: 'floating',
        placeholder: 'the price/how much',
        class: '',
        min: 1,
        max: 10000,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'textarea',
        label: 'Description',
        formControlName: 'description',
        dropdown: [],
        position: 'floating',
        placeholder: 'extra details',
        class: '',
        min: 1,
        max: 1000,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'file',
        label: 'Image Upload',
        formControlName: 'image',
        dropdown: [],
        position: 'floating',
        placeholder: 'image',
        class: '',
        min: 1,
        max: 1000,
      },

      {
        show: true,
        validators: ['required'],
        default: 'Drinks',
        type: 'select',
        label: 'Category',
        formControlName: 'category',
        dropdown: ['Drinks', 'Pastas', 'Desserts', 'Appetizers', 'K-Menu'],
        position: 'floating',
        placeholder: 'category of product',
        class: '',
        min: 1,
        max: 100,
      },
    ],
  };

  productForm: FormGroup;
  productData: any = this.adminService.data.product;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private uiService: UiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.setProductForm();
  }

  ionViewWillLeave() {
    this.adminService.setProductData(null);
  }

  setProductFormControlsWithData() {
    return this.settings.fields
      .map((field) => {
        const property = field.formControlName;
        const defaultValue = this.productData[property];
        const control = {
          [property]: this.fb.control(defaultValue),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setProductFormControls() {
    return this.settings.fields
      .map((field) => {
        const validators = field.validators.length ? Validators.required : null;
        const control = {
          [field.formControlName]: this.fb.control(field.default, validators),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setProductForm() {
    const productFormControls: any = this.productData
      ? this.setProductFormControlsWithData()
      : this.setProductFormControls();
    this.productForm = this.fb.group(productFormControls);
  }

  handleFileInput(files: FileList, formControlName) {
    this.productForm.controls[formControlName].setValue(files.item(0));
  }

  async uploadImage(image, imageName) {
    const response = await this.adminService.uploadImage(image, imageName);
  }

  async onSubmit() {
    if (this.productForm.status === 'INVALID') {
      this.uiService.presentToast("Some fields missin'", 'danger');
      return;
    }

    this.uiService.presentLoading();
    const product = { ...this.productForm.value };
    const data: any = {
      product: {
        ...product,
      },
      image: null,
    };
    data.product.price = parseFloat(data.product.price);
    if (data.product.image) {
      data.image = {
        category: 'main',
        placeholder: product.name,
        description: product.name,
      };
      data.image.name = '.' + data.product.image.name.split('.')[1];
    }

    // update product
    try {
      if (this.productData) {
        data.product.productId = this.productData.productId;
        data.image.imageId = this.productData.imageId;
        const response: any = await this.adminService.updateProduct(data);
        if (data.product.image.lastModified) {
          this.uploadImage(data.product.image, response.data.image.name);
        }
        this.uiService.dismissLoading();
        this.uiService.presentToast('Updated product');
        return;
      }
      const response: any = await this.adminService.insertProduct(data);
      if (data.product.image.lastModified) {
        this.uploadImage(data.product.image, response.data.image.name);
      }
      this.setProductForm();
      this.uiService.dismissLoading();
      this.uiService.presentToast('Saved product');
    } catch (error) {
      this.uiService.dismissLoading();
      this.uiService.presentToast(
        error.error.message || 'cannot save product',
        'warning'
      );
    }
  }
}
