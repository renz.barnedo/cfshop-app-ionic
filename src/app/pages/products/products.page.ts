import { Component, OnInit } from '@angular/core';
import {
  ActionSheetController,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { AdminService } from 'src/app/services/admin.service';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { ProductInfoPage } from '../product-info/product-info.page';
import { Router, NavigationExtras } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  products;

  constructor(
    private actionSheetController: ActionSheetController,
    private adminService: AdminService,
    private authService: AuthenticateService,
    private alertController: AlertController,
    private router: Router,
    private uiService: UiService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getProducts();
  }

  reorderItems(event) {
    const itemMove = this.products.splice(event.detail.from, 1)[0];
    this.products.splice(event.detail.to, 0, itemMove);
    this.products.forEach(async (product, index) => {
      index++;
      if (index !== product.sortOrder) {
        product.sortOrder = index++;
        try {
          const response: any = await this.adminService.updateProduct({
            product: {
              ...product,
            },
          });
          if (response.data) {
            this.uiService.presentToast('Reordered OK');
          }
        } catch (error) {
          this.uiService.presentToast('Cannot reorder');
        }
      }
    });
    event.detail.complete();
  }

  async getProducts(event = null) {
    await this.uiService.presentLoading();
    try {
      const response: any = await this.adminService.getProducts();
      this.products = response.data;
      this.uiService.dismissLoading();
      this.uiService.dismissRefreshContent(event);
    } catch (error) {
      this.uiService.dismissLoading();
      this.authService.displayErrorPage();
      this.uiService.dismissRefreshContent(event);
    }
  }

  async handleButtonClick(product) {
    const actionSheet = await this.actionSheetController.create({
      header: product.name,
      buttons: [
        {
          text: 'Full Info',
          icon: 'information-circle',
          handler: () => {
            this.openProductInfo(product);
          },
        },
        {
          text: 'Edit',
          icon: 'pencil',
          handler: () => {
            this.adminService.setProductData(product);
            this.router.navigateByUrl('products/post');
          },
        },
        {
          text: 'Delete',
          icon: 'trash',
          role: 'destructive',
          handler: async () => {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: `Delete`,
              message: `Are you sure you want to delete  ${product.name}?`,
              buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass: 'secondary',
                },
                {
                  text: 'Yes',
                  handler: async () => {
                    try {
                      let response = await this.adminService.deleteProduct(
                        product.productId
                      );
                      this.uiService.presentToast('deleted product');
                    } catch (error) {
                      this.uiService.presentToast(
                        'cannot delete product',
                        'danger'
                      );
                    }

                    this.getProducts();
                  },
                },
              ],
            });

            await alert.present();
          },
        },
      ],
    });

    await actionSheet.present();
  }

  async openProductInfo(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product,
      },
    };
    const name = product.name.split(' ').join('-').toLowerCase();
    this.router.navigate([`product/${name}`], navigationExtras);
  }
}
