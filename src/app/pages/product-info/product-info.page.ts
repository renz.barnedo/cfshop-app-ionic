import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { Meta } from '@angular/platform-browser';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.page.html',
  styleUrls: ['./product-info.page.scss'],
})
export class ProductInfoPage implements OnInit {
  product;
  show = false;
  isInMobile = window.screen.width < 426;

  tags = [
    { icon: 'heart', label: 'healthy' },
    { icon: 'happy', label: 'yummy' },
    { icon: 'thumbs-up', label: 'so good' },
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private authService: AuthenticateService,
    private meta: Meta,
    private uiService: UiService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        const passedData = this.router.getCurrentNavigation().extras.state
          .product;
        if (passedData) {
          this.show = true;
          this.product = passedData;
        }
      }
    });
  }

  async getProduct() {
    if (this.product) {
      return;
    }

    const productUrl = window.location.pathname;
    const productName = productUrl.split('/')[2];

    const response: any = await this.userService.getProductByName(productName);
    const productData = response.data;

    if (!productData) {
      this.authService.displayErrorPage();
    }
    this.product = productData;

    this.show = true;
  }

  async ngOnInit() {
    await this.getProduct();
  }

  async addToCart() {
    this.product.quantity = 1;
    this.product.isChecked = false;
    let cart = [];
    let alreadyInCart = false;
    if (localStorage.cart) {
      cart = JSON.parse(localStorage.cart);
      cart.forEach((item) => {
        if (item.productId === this.product.productId) {
          item.quantity++;
          alreadyInCart = true;
        }
      });
    }
    if (!alreadyInCart) {
      cart.push(this.product);
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    this.uiService.setCartTotalItems();
    this.uiService.presentToast(`Added ${this.product.name} to cart!`);
  }

  async shareProduct() {
    const navigatorApi: any = navigator;
    if (!navigatorApi.share) {
      this.uiService.presentToast(
        "Device doesn't support share feature!",
        'danger'
      );
    }
    try {
      const response: any = await navigatorApi.share({
        title: this.product.name,
        url: window.location.href,
        text: this.product.description,
      });
      if (response) {
        this.uiService.presentToast('Thanks for sharing...');
        return;
      }
      this.uiService.presentToast('Thanks for sharing!');
    } catch (error) {
      this.uiService.presentToast('Share failed', 'danger');
    }
  }

  copyLink() {
    const url = window.location.href;
    navigator.clipboard.writeText(url);
    this.uiService.presentToast('Copied product link');
  }
}
