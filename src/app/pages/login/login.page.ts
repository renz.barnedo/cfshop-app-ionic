import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup = this.fb.group({
    username: this.fb.control(null, Validators.required),
    password: this.fb.control(null, Validators.required),
  });
  isLoading: boolean = false;
  loading;
  logoSource =
    'https://api.renzbarnedo.codes/cfshop/v1/images/apple-touch-icon.png';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthenticateService,
    private uiService: UiService
  ) {}

  ionViewWillEnter() {
    this.checkLogIn();
  }

  checkLogIn() {
    if (localStorage.fb || localStorage.a) {
      this.uiService.presentToast("You're already logged in.");
      this.router.navigateByUrl('');
    }
  }

  ngOnInit() {
    this.checkLogIn();
  }

  async signInWithFb() {
    this.uiService.presentLoading();
    try {
      const fbData: any = await this.authService.fbLogUserIn();
      const response: any = await this.authService.fbLogUserInDatabase(fbData);
      this.uiService.dismissLoading();
      if (!fbData || !fbData.authToken) {
        return;
      }
      localStorage.setItem('fb', JSON.stringify(response.token));
      this.router.navigateByUrl('');
      this.uiService.presentToast("You're logged in.");
    } catch (error) {
      this.uiService.dismissLoading();
      this.uiService.presentToast(
        error.error.message || 'Cannot log in',
        'danger'
      );
    }
  }

  async submitLogin() {
    if (this.loginForm.status === 'INVALID') {
      this.uiService.presentToast('Invalid username or password', 'warning');
      return;
    }
    this.uiService.presentLoading();
    try {
      const data = this.loginForm.value;
      const response: any = await this.authService.logUserIn(data);

      this.loginForm.controls['username'].setValue(null);
      this.loginForm.controls['password'].setValue(null);

      localStorage.setItem('a', response.data.token);
      this.router.navigateByUrl('');

      this.uiService.dismissLoading();
      this.uiService.presentToast("You're logged in.");
    } catch (error) {
      this.uiService.dismissLoading();
      this.uiService.presentToast(error.error.message, 'danger');
    }
  }
}
