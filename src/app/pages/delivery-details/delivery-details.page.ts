import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-delivery-details',
  templateUrl: './delivery-details.page.html',
  styleUrls: ['./delivery-details.page.scss'],
})
export class DeliveryDetailsPage implements OnInit {
  deliveryDetailsForm: FormGroup;
  currentDetails;
  show = false;

  constructor(
    private modalController: ModalController,
    private fb: FormBuilder,
    private userService: UserService,
    private uiService: UiService
  ) {}

  async ngOnInit() {
    await this.setFormGroup();
    this.show = true;
  }

  setCurrentDeliveryDetails() {}

  async getUserDetails() {
    const response: any = await this.userService.getUserDeliveryDetails();
    if (response.data.length) {
      this.currentDetails = response.data[0];
    }
  }

  async setFormGroup() {
    const controls: any = {
      receiver: this.fb.control(null),
      address: this.fb.control(null),
      contactNumber: this.fb.control(null),
      instructions: this.fb.control(null),
    };

    await this.getUserDetails();
    if (this.currentDetails) {
      const current = this.currentDetails;
      controls.receiver.setValue(current.receiver);
      controls.address.setValue(current.address);
      controls.contactNumber.setValue(JSON.parse(current.contactNumbers)[0]);
    }
    this.deliveryDetailsForm = this.fb.group(controls);
  }

  async saveDeliveryDetails() {
    const form = this.deliveryDetailsForm.value;
    const data = {
      contact: {
        contactId: null,
        contactNumbers: [form.contactNumber],
        receiver: form.receiver,
        address: form.address,
      },
      instructions: form.instructions,
    };
    try {
      if (this.currentDetails && this.currentDetails.contactId) {
        data.contact.contactId = this.currentDetails.contactId;
        const response = await this.userService.updateUserDeliveryDetails(data);
        this.uiService.presentToast('Updated delivery details');
        this.dismissModal(data);
        return;
      }
      const response = await this.userService.insertUserDeliveryDetails(data);

      this.uiService.presentToast('Added delivery details');
      this.dismissModal(data);
    } catch (error) {
      const message =
        error && error.error.message
          ? error.error.message
          : 'Cannot save details';
      this.uiService.presentToast(message, 'danger');
    }
  }

  dismissModal(data = null) {
    this.modalController.dismiss(data, undefined);
  }
}
