import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {
  ActionSheetController,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { AdminService } from 'src/app/services/admin.service';
import { Router } from '@angular/router';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { ViewPage } from './view/view.page';
import { DatePipe } from '@angular/common';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {
  transactions;
  message: string;

  constructor(
    private actionSheetController: ActionSheetController,
    private adminService: AdminService,
    private authService: AuthenticateService,
    private router: Router,
    private modalController: ModalController,
    private uiService: UiService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getTransactions();
  }

  async getTransactions(event = null) {
    await this.uiService.presentLoading();
    try {
      const response: any = await this.adminService.getTransactions();
      this.uiService.dismissLoading();
      if (!response) {
        this.authService.changeErrorMessage('Cannot get transactions');
      }
      this.transactions = response.data.map((transaction) => {
        const { price, quantity } = transaction;
        const total: any = this.getTotal(price, quantity);

        transaction.pricing = `${this.setPhpDisplay(price)} × ${quantity} = `;
        transaction.total = this.setPhpDisplay(total);
        return transaction;
      });
      this.uiService.dismissRefreshContent(event);
    } catch (error) {
      this.uiService.dismissLoading();
      if (error.error.display) {
        this.authService.changeErrorMessage(error.error.display);
      }
      this.authService.displayErrorPage();
      this.uiService.dismissRefreshContent(event);
    }
  }

  @Output() redirect: EventEmitter<any> = new EventEmitter();

  async handleButtonClick(transaction) {
    const actionSheet = await this.actionSheetController.create({
      header: transaction.item,
      buttons: [
        {
          text: 'Full Info',
          icon: 'information-circle',
          handler: () => {
            this.openTransactionInfo(transaction);
          },
        },
        {
          text: 'Edit',
          icon: 'pencil',
          handler: () => {
            this.adminService.setTransactionData(transaction);
            this.router.navigateByUrl('transactions/post');
          },
        },
        {
          text: 'Delete',
          icon: 'trash',
          role: 'destructive',
          handler: async () => {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: `Delete`,
              message: `Are you sure you want to delete  ${transaction.item}?`,
              buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass: 'secondary',
                },
                {
                  text: 'Yes',
                  handler: async () => {
                    let response = await this.adminService.deleteTransaction(
                      transaction.transactionId
                    );
                    this.getTransactions();
                  },
                },
              ],
            });

            await alert.present();
          },
        },
      ],
    });

    await actionSheet.present();
  }

  getTotal(price, quantity) {
    return parseFloat(price) * parseInt(quantity);
  }

  setPhpDisplay(value) {
    return `₱${parseFloat(value).toFixed(2)}`;
  }

  async openTransactionInfo(transaction) {
    let details = { ...transaction };
    let total: any = this.getTotal(details.price, details.quantity);
    details.price = this.setPhpDisplay(details.price);
    details.quantity = `×${details.quantity} = ${this.setPhpDisplay(
      total
    )} (total)`;

    details.datetime = new DatePipe('en-US').transform(
      new Date(details.datetime),
      'MMMM dd, yyyy'
    );

    const transactionDetails = Object.keys(details)
      .filter(
        (key) =>
          key !== 'transactionId' &&
          key !== 'pricing' &&
          key !== 'total' &&
          key !== 'place'
      )
      .map((key) => ({ label: key, value: details[key] }));

    const modal = await this.modalController.create({
      component: ViewPage,
      cssClass: 'transactionModal',
      componentProps: {
        transactionDetails,
      },
    });
    return await modal.present();
  }
}
