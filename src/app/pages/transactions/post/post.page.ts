import { Component, OnInit, Input } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { AdminService } from 'src/app/services/admin.service';
import { UiService } from 'src/app/services/ui.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {
  settings = {
    fields: [
      {
        show: true,
        validators: ['required'],
        default: 'dateToday',
        type: 'date',
        label: 'Date',
        formControlName: 'datetime',
        dropdown: [],
        position: 'floating',
        placeholder: 'date of purchase',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: 'Ingredient',
        type: 'select',
        label: 'Category',
        formControlName: 'type',
        dropdown: ['Ingredient', 'Packaging', 'Transportation', 'Mobile Load'],
        position: 'floating',
        placeholder: 'platform used',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: 'Cash',
        type: 'select',
        label: 'Method',
        formControlName: 'wallet',
        dropdown: ['Cash', 'GCash', 'BDO', 'Paymaya', 'Coins.ph'],
        position: 'floating',
        placeholder: 'platform used',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: false,
        validators: ['required'],
        default: 'Cash',
        type: 'selectMethod',
        label: 'Method',
        formControlName: 'method',
        dropdown: [
          'Cash',
          'Online Prepaid',
          'Online Bank (debit)',
          'Online Bank (credit)',
          'Physical Card (debit)',
          'Physical Card (credit)',
          'Check',
        ],
        position: 'floating',
        placeholder: 'platform used',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: false,
        validators: ['required', 'disabled'],
        default: '',
        type: 'selectNested',
        label: 'Wallet',
        formControlName: 'wallet',
        dropdown: {
          Cash: ['Main wallet', 'Coin purse'],
          'Online Prepaid': ['GCash', 'PayMaya', 'Coins.ph'],
          'Online Bank (debit)': ['BDO', 'Metrobank', 'BPI'],
          'Online Bank (credit)': ['BDO', 'Metrobank', 'BPI'],
          'Physical Card (debit)': ['BDO', 'Metrobank', 'BPI'],
          'Physical Card (credit)': ['BDO', 'Metrobank', 'BPI'],
          Check: ['BDO', 'Metrobank', 'BPI'],
        },
        position: '',
        placeholder: 'fund used',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'Item',
        formControlName: 'item',
        dropdown: [],
        position: 'floating',
        placeholder: 'the product/service you purchased',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'number',
        label: 'Amount',
        formControlName: 'price',
        dropdown: [],
        position: 'floating',
        placeholder: 'the price/how much',
        class: '',
        min: 1,
        max: 100000000000,
      },
      {
        show: true,
        validators: ['required'],
        default: '1',
        type: 'number',
        label: 'Quantity',
        formControlName: 'quantity',
        dropdown: [],
        position: 'fixed',
        placeholder: 'number of items',
        class: '',
        min: 1,
        max: 100000,
      },
      {
        show: false,
        validators: [],
        default: '',
        type: 'text',
        label: 'Place',
        formControlName: 'place',
        dropdown: [],
        position: 'floating',
        placeholder: 'where you purchased the item',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'textarea',
        label: "add'l description",
        formControlName: 'description',
        dropdown: [],
        position: 'floating',
        placeholder: 'extra details',
        class: '',
        min: 1,
        max: 100,
      },
    ],
  };

  formatForDateToday: string = new DatePipe('en-US').transform(
    new Date(),
    'dd-MMMM-yyyy'
  );
  formatForTimeToday: string = new DatePipe('en-US').transform(
    new Date().setHours(new Date().getHours()),
    'dd-MMMM-yyyy hh:mm:ss a'
  );

  transactionData: any = this.adminService.data.transaction;
  transactionForm: FormGroup;
  quantity: any;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private uiService: UiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.setTransactionForm();
    // this.setWallet();
    this.quantity = this.transactionForm.controls.quantity;
  }

  setTransactionFormControlsWithData() {
    return this.settings.fields
      .map((field) => {
        const property = field.formControlName;
        const defaultValue = this.transactionData[property];
        const control = {
          [property]: this.fb.control(defaultValue),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setTransactionFormControls() {
    return this.settings.fields
      .map((field) => {
        if (field.default === 'dateToday') {
          field.default = this.formatForDateToday;
        }
        // if (field.default === 'time') {
        //   field.default = this.formatForTimeToday;
        // }
        const validators = field.validators.length ? Validators.required : null;
        const control = {
          [field.formControlName]: this.fb.control(field.default, validators),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setTransactionForm() {
    const transactionFormControls: any = this.transactionData
      ? this.setTransactionFormControlsWithData()
      : this.setTransactionFormControls();
    this.transactionForm = this.fb.group(transactionFormControls);
  }

  ionViewWillLeave() {
    this.adminService.setTransactionData(null);
  }

  setWallet() {
    const method: any = this.transactionForm.controls.method.value;
    const wallet: any = this.transactionForm.controls.wallet;

    wallet.value = this.settings.fields.find(
      (field) => field.formControlName === 'wallet'
    ).dropdown[method][0];

    this.transactionForm.controls['wallet'].setValue(wallet.value);
  }

  addQuantity() {
    this.quantity.value++;
    this.transactionForm.controls['quantity'].setValue(this.quantity.value);
  }

  subtractQuantity() {
    if (this.quantity.value > 1) {
      this.quantity.value--;
      this.transactionForm.controls['quantity'].setValue(this.quantity.value);
    }
  }

  async onSubmit() {
    if (this.transactionForm.status === 'INVALID') {
      this.uiService.presentToast("Some fields missin'", 'danger');
      return;
    }

    this.uiService.presentLoading();
    const data = {
      transaction: {
        ...this.transactionForm.value,
      },
    };
    data.transaction.price = parseFloat(data.transaction.price);

    try {
      if (this.transactionData) {
        data.transaction.transactionId = this.transactionData.transactionId;
        const response = await this.adminService.updateTransaction(data);
        this.uiService.dismissLoading();
        this.uiService.presentToast('Updated transaction');
        return;
      }
      const response = await this.adminService.insertTransaction(data);
      this.setTransactionForm();
      this.uiService.dismissLoading();
      this.uiService.presentToast('Added transaction');
    } catch (error) {
      this.uiService.dismissLoading();
      this.uiService.presentToast("Can't submit", 'danger');
    }
  }
}
