import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { CheckoutPage } from '../checkout/checkout.page';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UiService } from 'src/app/services/ui.service';
import { NavigationExtras, Router } from '@angular/router';
import { AuthenticateService } from 'src/app/services/authenticate.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  cartForm: FormGroup;
  cart = {
    items: [],
    total: 0,
    checkboxAll: false,
  };

  constructor(
    private modalController: ModalController,
    private fb: FormBuilder,
    private uiService: UiService,
    private alertController: AlertController,
    private router: Router,
    private authService: AuthenticateService
  ) {}

  ngOnInit() {
    this.getCartItems();
    this.setCartForm();
  }

  setCartForm() {
    const formArray = this.cart.items.map((item) =>
      this.fb.group({
        productId: item.productId,
        isChecked: this.fb.control(item.isChecked),
        quantity: this.fb.control(item.quantity),
      })
    );
    this.cartForm = this.fb.group({
      products: this.fb.array(formArray),
    });
  }

  ionViewWillEnter() {
    this.getCartItems();
    this.setCartForm();
  }

  getCartItems() {
    if (localStorage.cart) {
      this.cart.items = JSON.parse(localStorage.cart);
      this.checkCheckboxes();
      this.computeTotal();
    }
  }

  checkCheckboxes() {
    const checkedItems = this.cart.items.filter((item) => item.isChecked);
    this.cart.checkboxAll = checkedItems.length === this.cart.items.length;
  }

  toggleCheckbox(index) {
    const item = this.cart.items[index];
    item.isChecked = !item.isChecked;
    this.checkCheckboxes();
    this.setCart();
  }

  toggleCheckboxAll() {
    const products: any = this.cartForm.get('products');
    this.cart.items.forEach((item, index) => {
      products.controls[index].controls.isChecked.setValue(
        !this.cart.checkboxAll
      );
      item.isChecked = !this.cart.checkboxAll;
      return item;
    });
    this.setCart();
  }

  computeCheckedItemsTotal() {
    this.cart.total = this.cart.items
      .filter((item) => item.isChecked)
      .reduce(
        (total, item) => total + parseFloat(item.price) * item.quantity,
        0
      );
  }

  subtractQuantity(index) {
    const products: any = this.cartForm.get('products');
    const item = this.cart.items[index];
    const control = products.controls[index];
    if (item.quantity > 1) {
      item.quantity--;
      control.value.quantity--;
      this.setCart();
      this.uiService.setCartTotalItems();
      return;
    }
    this.uiService.presentToast('Cannot set 0 pc', 'warning');
  }

  addQuantity(index) {
    const products: any = this.cartForm.get('products');
    const item = this.cart.items[index];
    const control = products.controls[index];
    if (item.quantity < 100) {
      item.quantity++;
      control.value.quantity++;
      this.setCart();
      this.uiService.setCartTotalItems();
      return;
    }
    this.uiService.presentToast('Cannot set 100+ pcs', 'warning');
  }

  setCart() {
    localStorage.cart = JSON.stringify(this.cart.items);
    this.computeTotal();
  }

  computeTotal() {
    this.cart.total = this.cart.items
      .filter((item) => item.isChecked)
      .reduce(
        (total, item) => total + parseFloat(item.price) * item.quantity,
        0
      );
  }

  async openProductInfo(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product,
      },
    };
    const name = product.name.split(' ').join('-').toLowerCase();
    this.router.navigate([`product/${name}`], navigationExtras);
  }

  async removeFromCart(item, index) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Remove Item',
      message: `Are you sure you remove <strong>${item.name}</strong> from cart?`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Yes',
          handler: () => {
            this.cart.items.splice(index, 1);
            this.setCart();
            this.uiService.setCartTotalItems();
            this.ionViewWillEnter();
            this.uiService.presentToast(`Removed ${item.name} from cart`);
          },
        },
      ],
    });

    await alert.present();
  }

  async checkoutClicked() {
    try {
      const fbIsVerified = await this.authService.verifyFbToken();
      const userIsVerified = await this.authService.verifyToken();
      if (fbIsVerified && userIsVerified) {
        this.openCheckout();
      }
    } catch (error) {
      localStorage.removeItem('fb');
      this.router.navigateByUrl('login');
      this.uiService.presentToast('You need to login first.', 'warning');
    }
  }

  async openCheckout() {
    const cart = {
      total: this.cart.total,
      items: this.cart.items.filter((item) => item.isChecked),
    };
    if (!cart.items.length) {
      this.uiService.presentToast('Check items first!', 'warning');
      return;
    }
    localStorage.checkout = JSON.stringify(cart);
    this.router.navigateByUrl('checkout');
    // let navigationExtras: NavigationExtras = {
    //   state: {
    //     cart,
    //   },
    // };
    // this.router.navigate([`checkout`], navigationExtras);
  }
}
