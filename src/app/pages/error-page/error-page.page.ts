import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from 'src/app/services/authenticate.service';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.page.html',
  styleUrls: ['./error-page.page.scss'],
})
export class ErrorPagePage implements OnInit {
  errorMessage;
  constructor(private authService: AuthenticateService) {}

  ngOnInit() {
    this.authService.errorMessage.subscribe((message) => {
      this.errorMessage = message;
    });
  }
}
