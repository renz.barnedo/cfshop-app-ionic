import { Component, OnInit } from '@angular/core';
import {
  MenuController,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { FilterPage } from '../filter/filter.page';
import { LoginPage } from '../login/login.page';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { UiService } from 'src/app/services/ui.service';
import { SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-header',
  templateUrl: './header.page.html',
  styleUrls: ['./header.page.scss'],
})
export class HeaderPage implements OnInit {
  public selectedPage = '/' + window.location.href.split('/')[3];
  deferredPrompt;
  isInMobile = window.screen.width < 426;

  menu = {
    title: 'Cruzine',
    subtitle: 'cruzine.cafe',
    socials: [
      { url: 'https://twitter.com/cafecruzine', icon: 'logo-twitter' },
      { url: 'https://www.instagram.com/cafecruzine', icon: 'logo-instagram' },
      { url: 'https://www.facebook.com/cafecruzine', icon: 'logo-facebook' },
    ],
    sections: [
      {
        show: true,
        label: 'Navigation',
        pages: [
          {
            title: 'Home',
            url: '/home',
            icon: 'home',
            show: true,
          },
          {
            title: 'Log in',
            url: '/login',
            icon: 'log-in',
            show: true,
          },
          {
            title: 'Account',
            url: '/account',
            icon: 'person-circle',
            show: false,
          },
          {
            title: 'Log out',
            url: '/logout',
            icon: 'log-out',
            show: false,
            handler: async () => {
              const alert = await this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Log Out',
                message: 'Are you sure you want to log out?',
                buttons: [
                  {
                    text: 'No',
                    role: 'cancel',
                    cssClass: 'secondary',
                  },
                  {
                    text: 'Yes',
                    handler: () => {
                      this.signOutFb();
                      this.setSelectedPage();
                      this.hideLoggedInIcons();
                      this.router.navigateByUrl('/');
                      this.uiService.presentToast("You're logged out.");
                    },
                  },
                ],
              });

              await alert.present();
            },
          },
        ],
      },
      {
        show: true,
        label: 'Install',
        pages: [
          {
            title: 'Add to Home screen',
            url: '/addtohomescreen',
            icon: 'apps',
            show: true,
            handler: () => {
              if (!this.deferredPrompt) {
                this.uiService.presentToast('Thanks for adding to homescreen!');
                return;
              }
              this.deferredPrompt.prompt();
              this.deferredPrompt.userChoice.then((choiceResult) => {
                if (choiceResult.outcome === 'accepted') {
                  this.uiService.presentToast(
                    'Thanks for adding to homescreen!',
                    'success',
                    'top',
                    3000
                  );
                } else {
                  this.uiService.presentToast(
                    'Add to homescreen failed!',
                    'danger'
                  );
                }
              });
            },
          },
        ],
      },
      {
        show: false,
        label: 'Admin',
        pages: [
          {
            title: 'Users',
            url: '/users',
            icon: 'people',
            show: true,
          },
          {
            title: 'Orders',
            url: '/deliveries',
            icon: 'receipt',
            show: true,
          },
          {
            title: 'Transactions',
            url: '/transactions',
            icon: 'pricetags',
            show: true,
          },
          {
            title: 'Promos',
            url: '/promos',
            icon: 'happy',
            show: false,
          },
          {
            title: 'Products',
            url: '/products',
            icon: 'wine',
            show: true,
          },
          {
            title: 'Announcements',
            url: '/announcements',
            icon: 'mic',
            show: false,
          },
          {
            title: 'Statistics',
            url: '/statistics',
            icon: 'stats-chart',
            show: true,
          },
        ],
      },
      {
        show: true,
        label: 'Information',
        pages: [
          {
            title: 'Help',
            url: '/help',
            icon: 'help-circle',
            show: true,
          },
          {
            title: 'About',
            url: '/about',
            icon: 'information-circle',
            show: true,
          },
        ],
      },
    ],
  };

  title: string;
  page: string;
  show: boolean = true;

  constructor(
    private modalController: ModalController,
    private router: Router,
    private alertController: AlertController,
    private authService: AuthenticateService,
    private uiService: UiService,
    private socialAuthService: SocialAuthService
  ) {
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        const child = window.location.pathname.split('/')[2];
        this.page = window.location.pathname.split('/')[1];
        this.show = this.page !== 'product' && this.page !== 'checkout' ? true : false;
        this.page = child ? `${this.page} - ${child}` : this.page;
        if (this.page === 'home') {
          this.title = 'cruzine.cafe';
          return;
        }
        this.title = this.page
          .split('')
          .map((letter, index) => (index === 0 ? letter.toUpperCase() : letter))
          .join('');
      }
    });
  }

  ngOnInit() {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
      this.showInstallPromotion();
    });
    window.addEventListener('appinstalled', (evt) => {
      this.uiService.presentToast('Add to homescreen success!');
    });
    window.addEventListener('DOMContentLoaded', () => {
      let displayMode = 'browser tab';
      const navigatorApi: any = navigator;
      if (navigatorApi.standalone) {
        displayMode = 'standalone-ios';
      }
      if (window.matchMedia('(display-mode: standalone)').matches) {
        displayMode = 'standalone';
      }
      if (displayMode.indexOf('standalone') > -1) {
        this.uiService.presentToast('Thanks for adding to home screen!');
        this.menu.sections = this.menu.sections.map((section) => {
          if (section.label === 'Install') {
            section.show = false;
          }
          return section;
        });
      }
    });
  }

  showInstallPromotion() {
    this.uiService.presentToast('You can add this website to homescreen!');
  }

  onItemClick(item) {
    if (item.hasOwnProperty('handler')) {
      item.handler();
      return;
    }
    this.router.navigateByUrl(item.url);
  }

  setSelectedPage() {
    this.selectedPage = '/' + window.location.href.split('/')[3];
    // const pageUrl = '/' + window.location.href.split('/')[3];
    // this.selectedPage = item.url == '/logout' ? pageUrl : item.url;
  }

  async signOutFb() {
    if (!localStorage.fb) {
      return;
    }
    this.socialAuthService.signOut();
  }

  async openMenu() {
    this.setSelectedPage();

    const userHasFbToken = this.authService.checkUserFbToken();
    if (userHasFbToken) {
      const verify: any = await this.authService.verifyFbToken();

      if (verify.data.type) {
        this.showLoggedInIconsForUser();
        return;
      }
      this.hideLoggedInIcons();
      return;
    }

    const userHasToken = this.authService.checkUserToken();
    if (!userHasToken) {
      this.hideLoggedInIcons();
      return;
    }
    try {
      const verifyToken = await this.authService.verifyToken();
      if (verifyToken) {
        this.showLoggedInIconsForAdmin();
      }
    } catch (error) {
      this.hideLoggedInIcons();
    }
  }

  showLoggedInIconsForUser() {
    this.menu.sections.forEach((section) => {
      // this.toggleUserIcons(section, true);
      this.toggleAuthenticateIcons(section, false);
    });
  }

  showLoggedInIconsForAdmin() {
    this.menu.sections.forEach((section) => {
      this.toggleAdminIcons(section, true);
      this.toggleAuthenticateIcons(section, false);
    });
  }

  toggleUserIcons(section, show) {
    if (section.label === 'User') {
      section.show = show;
    }
  }

  toggleAdminIcons(section, show) {
    if (section.label === 'Admin') {
      section.show = show;
    }
  }

  toggleAuthenticateIcons(section, show) {
    if (section.label === 'Navigation') {
      section.pages.forEach((page) => {
        if (page.title === 'Log in') {
          page.show = show;
        }
        if (page.title === 'Log out') {
          page.show = !show;
        }
      });
    }
  }

  hideLoggedInIcons() {
    localStorage.removeItem('a');
    localStorage.removeItem('fb');
    this.menu.sections.forEach((section) => {
      this.toggleAdminIcons(section, false);
      this.toggleAuthenticateIcons(section, true);
    });
  }

  async shareWebsite() {
    const navigatorApi: any = navigator;
    if (!navigatorApi.share) {
      this.uiService.presentToast(
        "Device doesn't support share feature!",
        'danger'
      );
    }
    try {
      const response: any = await navigatorApi.share({
        title: 'Cafe Cruzine',
        url: window.location.href,
        text: 'Healthy foods and drinks for you! Support small Filipino businesses.',
      });
      if (response) {
        this.uiService.presentToast('Thanks for sharing...');
        return;
      }
      this.uiService.presentToast('Thanks for sharing!');
    } catch (error) {
      this.uiService.presentToast('Share failed', 'danger');
    }
  }

  openUrl(url) {
    window.open(url, '_blank');
  }
}
