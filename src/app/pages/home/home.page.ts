import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { ProductInfoPage } from '../product-info/product-info.page';
import { UserService } from 'src/app/services/user.service';
import { UiService } from 'src/app/services/ui.service';
import { Router, NavigationExtras } from '@angular/router';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  products;
  columnsCount;
  displayFilter;
  _subscription;
  constructor(
    private toastController: ToastController,
    private userService: UserService,
    private uiService: UiService,
    private router: Router,
    private meta: Meta
  ) {
    this._subscription = this.userService.nameChange.subscribe((value) => {
      this.columnsCount = JSON.parse(localStorage.filter)[1];
      const products = this.userService.productsList;
      this.filterProducts(products);
    });
  }

  ionViewWillEnter() {
    this.columnsCount = localStorage.filter
      ? JSON.parse(localStorage.filter)[1]
      : null;
  }

  ngOnInit() {
    this.getProducts();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  async getProducts(event = null) {
    await this.uiService.presentLoading();
    try {
      const response: any = await this.userService.getProducts();
      this.setDisplayFilter(response.data);
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
    } catch (error) {
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
      this.uiService.presentToast("Can't get products", 'danger');
    }
  }

  filterProducts(products) {
    const category = JSON.parse(localStorage.filter)[0].split(' ')[0];
    this.products =
      category === 'All'
        ? products
        : products.filter((products) => products.category === category);
  }

  setDisplayFilter(products) {
    if (!localStorage.filter) {
      return;
    }
    this.userService.productsList = products;
    this.columnsCount = JSON.parse(localStorage.filter)[1];
    this.filterProducts(products);
  }

  async openProductInfo(product) {
    let navigationExtras: NavigationExtras = {
      state: {
        product,
      },
    };
    const name = product.name.split(' ').join('-').toLowerCase();
    this.router.navigate([`product/${name}`], navigationExtras);
  }

  async addToCart(product) {
    product.quantity = 1;
    product.isChecked = false;
    let cart = [];
    let alreadyInCart = false;
    if (localStorage.cart) {
      cart = JSON.parse(localStorage.cart);
      cart.forEach((item) => {
        if (item.productId === product.productId) {
          item.quantity++;
          alreadyInCart = true;
        }
      });
    }
    if (!alreadyInCart) {
      cart.push(product);
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    this.uiService.setCartTotalItems();
    this.uiService.presentToast(`Added ${product.name} to cart!`);
  }
}
