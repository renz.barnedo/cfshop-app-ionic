import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { NavigationExtras, Router } from '@angular/router';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-deliveries',
  templateUrl: './deliveries.page.html',
  styleUrls: ['./deliveries.page.scss'],
})
export class DeliveriesPage implements OnInit {
  statuses = ['processing', 'on-delivery', 'delivered'];
  status = this.statuses[0];
  show: boolean = false;
  deliveries;

  constructor(
    private adminService: AdminService,
    private actionSheetController: ActionSheetController,
    private router: Router,
    private uiService: UiService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getStatusOrders();
    this.show = true;
  }

  async getStatusOrders(event = null) {
    try {
      const response: any = await this.adminService.getStatusOrders(
        this.status
      );
      this.deliveries = response.data;
      this.uiService.dismissRefreshContent(event);
    } catch (error) {
      const message = error.error.message || 'Cannot get orders';
      this.uiService.presentToast(message, 'danger');
      this.uiService.dismissRefreshContent(event);
    }
  }

  async handleButtonClick(delivery) {
    const actionSheet = await this.actionSheetController.create({
      header: delivery.datetime,
      buttons: [
        {
          text: 'Edit',
          icon: 'pencil',
          handler: () => {
            const user = {
              ...delivery.user,
            };
            const passedDelivery = {
              ...delivery,
            };
            delete delivery.user;

            let navigationExtras: NavigationExtras = {
              state: {
                user: {
                  ...user,
                  delivery: passedDelivery,
                },
              },
            };

            this.router.navigate(
              [`users/order/${user.userId}`],
              navigationExtras
            );
          },
        },
        {
          text: 'Delete',
          icon: 'trash',
          role: 'destructive',
          handler: async () => {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: `Delete`,
              message: `Are you sure you want to delete  ${delivery.datetime}?`,
              buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass: 'secondary',
                },
                {
                  text: 'Yes',
                  handler: async () => {
                    try {
                      let response: any = await this.adminService.deleteOrder(
                        delivery.deliveryId
                      );
                      if (response.data) {
                        this.uiService.presentToast('Deleted order');
                        // this.getOrders(this.user.userId);
                        return;
                      }
                      this.uiService.presentToast(
                        'cannot delete db order',
                        'danger'
                      );
                    } catch (error) {
                      this.uiService.presentToast(
                        'cannot delete order',
                        'danger'
                      );
                    }
                  },
                },
              ],
            });

            await alert.present();
          },
        },
      ],
    });

    await actionSheet.present();
  }
}
