import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  showTabs: boolean = true;
  items: number = 0;
  clickEventsubscription: Subscription;

  constructor(private router: Router, private uiService: UiService) {
    this.clickEventsubscription = this.uiService
      .getClickEvent()
      .subscribe(() => {
        this.setCartTotalItems();
      });
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.showTabs = true;
        const path = window.location.pathname.split('/')[1];
        if (path === 'checkout' || path === 'login') {
          this.showTabs = false;
        }
      }
    });
  }

  ngOnInit() {
    this.setCartTotalItems();
  }

  setCartTotalItems() {
    if (localStorage.cart) {
      this.items = JSON.parse(localStorage.cart).reduce(
        (total, item) => total + item.quantity,
        0
      );
    }
  }
}
