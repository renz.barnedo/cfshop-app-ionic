import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DeliveryDetailsPage } from '../delivery-details/delivery-details.page';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
  cart;
  totalItems: number;
  show = false;
  contact;
  instructions;

  constructor(
    private modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private uiService: UiService
  ) {
    // this.activatedRoute.queryParams.subscribe((params) => {
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     const passedData = this.router.getCurrentNavigation().extras.state.cart;
    //     this.cart = passedData;
    //     this.checkCart();
    //   }
    // });
  }

  async ionViewWillEnter() {
    this.cart = localStorage.checkout
      ? JSON.parse(localStorage.checkout)
      : null;
    this.checkCart();
    await this.getUserDetails();
  }

  ngOnInit() {}

  async getUserDetails() {
    try {
      const response: any = await this.userService.getUserDeliveryDetails();

      if (response.data.length) {
        this.contact = response.data[0];
        this.contact.number = JSON.parse(this.contact.contactNumbers)[0];
      }
      this.show = true;
    } catch (error) {
      let message =
        error.error && error.error.message
          ? error.error.message
          : 'cannot get receiver details';
      if (message === 'jwt expired') {
        message = 'session expired, login again';
        this.router.navigateByUrl('login');
      }
      this.uiService.presentToast(message, 'danger');
    }
  }

  checkCart() {
    if (!this.cart || !this.cart.items.length) {
      this.router.navigateByUrl('cart');
      return;
    }
    this.totalItems = this.cart.items.reduce(
      (total, item) => total + item.quantity,
      0
    );
  }

  async editContactDetails() {
    const modal = await this.modalController.create({
      component: DeliveryDetailsPage,
    });

    await modal.present();
    modal
      .onWillDismiss()
      .then((response) => {
        this.instructions = response.data.instructions || '';
        this.getUserDetails();
      })
      .catch((error) => {
      });
  }

  async placeOrder() {
    // TODO: check if has contact details
    if (!this.contact || !this.contact.contactId) {
      this.uiService.presentToast('Update Receiver Details first!', 'warning');
      this.editContactDetails();
      return;
    }
    const data = {
      contact: {
        contactId: this.contact.contactId,
      },
      delivery: {
        userId: this.contact.userId,
        status: 'Processing',
        courier: '',
        instructions: this.instructions || '',
      },
      orders: this.cart.items.map((item) => ({
        productId: item.productId,
        quantity: item.quantity,
      })),
    };
    try {
      const response = await this.userService.insertOrder(data);
      this.uiService.presentToast('Placed order!');
      this.router.navigateByUrl('orders');

      localStorage.removeItem('checkout');
      const cart = JSON.parse(localStorage.cart).filter(
        (cartItem, index) =>
          !this.cart.items.find(
            (checkoutItem) => checkoutItem.productId === cartItem.productId
          )
      );
      localStorage.cart = JSON.stringify(cart);

      this.uiService.setCartTotalItems();
    } catch (error) {
      this.uiService.presentToast('Cannot place order');
    }
  }
}
