import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  constructor(private uiService: UiService) {}

  ngOnInit() {}

  onSubmit() {
    this.uiService.presentToast('This page is still a work in progress!');
  }
}
