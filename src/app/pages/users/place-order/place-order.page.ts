import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.page.html',
  styleUrls: ['./place-order.page.scss'],
})
export class PlaceOrderPage implements OnInit {
  settings = {
    fields: [
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: "Receiver's Full Name",
        formControlName: 'receiver',
        dropdown: [],
        position: 'stacked',
        placeholder: 'receiver',
        class: '',
        min: 1,
        max: 100000,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'number',
        label: 'Contact Number',
        formControlName: 'contactNumber',
        dropdown: [],
        position: 'stacked',
        placeholder: 'contact',
        class: '',
        min: 1,
        max: 100000,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'Full Address',
        formControlName: 'address',
        dropdown: [],
        position: 'stacked',
        placeholder: 'address',
        class: '',
        min: 1,
        max: 100000,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'text',
        label: 'Instructions',
        formControlName: 'instructions',
        dropdown: [],
        position: 'stacked',
        placeholder: 'instructions',
        class: '',
        min: 1,
        max: 100000,
      },
      {
        show: true,
        validators: ['required'],
        default: 'dateToday',
        type: 'date',
        label: 'Date',
        formControlName: 'date',
        items: [{}],
        position: 'stacked',
        placeholder: 'date of purchase',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: 'timeToday',
        type: 'time',
        label: 'Time',
        formControlName: 'time',
        items: [],
        position: 'stacked',
        placeholder: 'time of purchase',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: 'Processing',
        type: 'select',
        label: 'Status',
        formControlName: 'type',
        items: ['Processing', 'On Delivery', 'Delivered', 'Canceled'],
        position: 'stacked',
        placeholder: 'category',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'number',
        label: 'Shipping Fee',
        formControlName: 'shippingFee',
        dropdown: [],
        position: 'stacked',
        placeholder: "the price of courier's delivery fee",
        class: '',
        min: 1,
        max: 100000,
      },
    ],
  };

  user;
  show = false;
  orderData: any = this.adminService.data.order;
  orderForm: FormGroup;
  total;
  contact;

  formatForDateToday: string = new DatePipe('en-US').transform(
    new Date(),
    'dd-MMMM-yyyy'
  );
  formatForTimeToday: string = new DatePipe('en-US').transform(
    new Date().setHours(new Date().getHours()),
    'dd-MMMM-yyyy hh:mm:ss a'
  );

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private adminService: AdminService,
    private uiService: UiService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        const passedData = this.router.getCurrentNavigation().extras.state.user;
        if (passedData) {
          this.user = passedData;
        }
      }
    });
  }

  async ngOnInit() {
    this.contact = this.user;
    await this.getProducts();
    await this.getContacts();
    this.setOrderForm();
    this.getUser();
    this.computeTotal();
    this.show = true;
  }

  getUser() {
    if (this.user) {
      return;
    }

    const userId = window.location.pathname.split('/')[3];

    // TODO: get user with userID (findByPK)
  }

  async getContacts() {
    try {
      const response: any = await this.adminService.getUserContact(
        this.user.userId
      );
      if (response.data.length) {
        this.contact = response.data[0];
        this.contact.cellphone = JSON.parse(this.contact.contactNumbers)[0];
        this.contact.fullName = this.contact.receiver;
      }
    } catch (error) {}
    // TODO: get user contacts then set it to defaults if existing
    // TODO: if not, defaults would be user's fullname, address, cellphone
  }

  async getProducts() {
    try {
      const response: any = await this.adminService.getProducts();
      const field = {
        show: true,
        validators: [],
        default: null,
        type: 'checkbox',
        label: '',
        formControlName: 'orders',
        items: response.data.map((product) => {
          product.quantity = 1;
          return product;
        }),
        position: 'stacked',
        placeholder: 'orders',
        class: '',
        min: 1,
        max: 100,
      };
      this.settings.fields.push(field);
    } catch (error) {
      this.uiService.presentToast(
        error.error.message || 'Cannot get products',
        'danger'
      );
    }
  }

  setOrderFormControlsWithData() {
    const productsField: any = this.settings.fields.slice(-1);
    return this.settings.fields
      .map((field) => {
        if (field.default === 'dateToday') {
          field.default = new DatePipe('en-US').transform(
            new Date(this.user.delivery.datetime),
            'dd-MMMM-yyyy'
          );
        }
        if (field.default === 'timeToday') {
          field.default = new DatePipe('en-US').transform(
            new Date(this.user.delivery.datetime).setHours(
              new Date(this.user.delivery.datetime).getHours()
            ),
            'dd-MMMM-yyyy hh:mm:ss a'
          );
        }

        if (field.formControlName === 'shippingFee') {
          field.default = this.user.delivery.shippingFee;
        }
        if (field.default === 'Processing') {
          field.default = this.user.delivery.status;
        }

        if (field.formControlName === 'receiver') {
          field.default = this.contact.fullName;
        }
        if (field.formControlName === 'contactNumber') {
          field.default = this.contact.cellphone;
        }
        if (field.formControlName === 'address') {
          field.default = this.contact.address;
        }
        if (field.formControlName === 'instructions') {
          field.default = this.contact.instructions;
        }
        const validators = field.validators.length ? Validators.required : null;
        if (field.type === 'checkbox') {
          let items: any = field.items;

          const controls = items.map((item, index) => {
            let initial = null;

            if (this.user.delivery) {
              initial = this.user.delivery.orders.find(
                (product) => product.productId === item.productId
              );

              if (initial) {
                productsField[0].items.forEach((item) => {
                  if (item.productId === initial.productId) {
                    item.quantity = initial.quantity;
                  }
                });
              }
            }
            const control = this.fb.control(initial, validators);
            return control;
          });

          return {
            orders: this.fb.array(controls),
          };
        }
        const control = {
          [field.formControlName]: this.fb.control(field.default, validators),
        };

        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setOrderFormControls() {
    return this.settings.fields
      .map((field) => {
        if (field.default === 'dateToday') {
          field.default = this.formatForDateToday;
        }
        if (field.default === 'timeToday') {
          field.default = this.formatForTimeToday;
        }
        if (field.formControlName === 'receiver') {
          field.default = this.contact.fullName;
        }
        if (field.formControlName === 'contactNumber') {
          field.default = this.contact.cellphone;
        }
        if (field.formControlName === 'address') {
          field.default = this.contact.address;
        }
        const validators = field.validators.length ? Validators.required : null;
        if (field.type === 'checkbox') {
          let controls: any = field.items;
          controls = controls.map(() => this.fb.control(null, validators));
          const orders = {
            orders: this.fb.array(controls),
          };
          return orders;
        }
        const control = {
          [field.formControlName]: this.fb.control(field.default, validators),
        };

        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setOrderForm() {
    const OrderFormControls: any = this.user.delivery
      ? this.setOrderFormControlsWithData()
      : this.setOrderFormControls();

    this.orderForm = this.fb.group(OrderFormControls);
  }

  addQuantity(index) {
    const fields = this.settings.fields.slice(-1);
    const products = fields[0].items;
    const product: any = products[index];
    product.quantity++;
    this.computeTotal();
  }

  subtractQuantity(index) {
    const fields = this.settings.fields.slice(-1);
    const products = fields[0].items;
    const product: any = products[index];
    if (product.quantity < 2) {
      // TODO: notif
      return;
    }
    product.quantity--;
    this.computeTotal();
  }

  computeTotal() {
    const orders = this.orderForm.value.orders;
    const products: any = this.settings.fields.slice(-1)[0].items;
    const total = products.reduce((total, product, index) => {
      let price = 0;
      let quantity = 0;
      const productIsChecked = orders[index];
      if (productIsChecked) {
        price = parseFloat(product.price);
        quantity = parseInt(product.quantity);
      }
      const productOfNumbers = parseFloat((price * quantity).toFixed(2));
      return total + productOfNumbers;
    }, 0);

    this.total = total;
  }

  async insertOrder(data) {
    let contact = { ...this.contact };
    if (!this.contact.contactId) {
      contact = await this.insertContact();
    }
    data = {
      ...data,
      contact: {
        contactId: contact.contactId,
      },
    };
    try {
      const response = await this.adminService.insertOrder(data);
      this.uiService.presentToast('Added order');
      this.router.navigateByUrl('users');
    } catch (error) {
      const message =
        (error.error && error.error.message) || 'Cannot insert order';
      this.uiService.presentToast(message);
    }
  }

  async updateOrder(data) {
    data = {
      ...data,
      contact: {
        contactId: this.contact.contactId,
      },
    };
    try {
      const response = await this.adminService.updateOrder(data);
      this.uiService.presentToast('Updated order');
    } catch (error) {
      this.uiService.presentToast(error.error.message || 'Cannot update order');
    }
  }

  async updateContact() {
    const form = this.orderForm.value;
    const contact = {
      contact: {
        contactId: this.contact.contactId,
        userId: this.user.userId,
        contactNumbers: [form.contactNumber],
        receiver: form.receiver,
        address: form.address,
      },
    };
    try {
      const response = await this.adminService.updateUserContact(contact);
      this.uiService.presentToast('Added contact');
      return response;
    } catch (error) {
      this.uiService.presentToast('Cannot add contact', 'warning');
      return null;
    }
  }

  async insertContact() {
    const form = this.orderForm.value;
    const contact = {
      contact: {
        userId: this.user.userId,
        contactNumbers: [form.contactNumber],
        receiver: form.receiver,
        address: form.address,
      },
    };
    try {
      const response: any = await this.adminService.insertUserContact(contact);
      this.contact = response.contact;
      this.uiService.presentToast('Added contact');
      return response.contact;
    } catch (error) {
      this.uiService.presentToast('Cannot add contact', 'warning');
      return null;
    }
  }

  async onSubmit() {
    if (this.orderForm.status === 'INVALID') {
      this.uiService.presentToast('Missing fields', 'warning');
      return;
    }

    // validate
    // dialog popup confirm
    const formValues = this.orderForm.value;
    const { date, time, type, shippingFee } = formValues;
    const fields = this.settings.fields.slice(-1);
    const products: any = fields[0].items;

    let orders = [...formValues.orders]
      .map((order, index) =>
        order
          ? {
              productId: products[index].productId,
              quantity: products[index].quantity,
            }
          : null
      )
      .filter((order) => order);

    const { userId, cellphone, telephone, address } = this.user;
    let data = {
      delivery: {
        userId: this.user.userId,
        status: type.includes(' ') ? type.replace(/-/g, '-') : type,
        shippingFee: shippingFee ? parseFloat(shippingFee) : null,
        datetime:
          new DatePipe('en-US').transform(date, 'yyyy-MM-dd') +
          ' ' +
          new DatePipe('en-US').transform(time, 'hh:mm:ss a'),
      },
      orders,
    };

    if (this.user.hasOwnProperty('delivery')) {
      // data.delivery.deliveryId = this.user.delivery.deliveryId;
      const updateData = {
        delivery: {
          ...data.delivery,
          deliveryId: this.user.delivery.deliveryId,
        },
        orders: data.orders,
      };
      this.updateContact();
      this.updateOrder(updateData);
      return;
    }

    this.insertOrder(data);
  }
}
