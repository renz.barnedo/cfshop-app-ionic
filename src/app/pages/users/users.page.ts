import { Component, OnInit } from '@angular/core';
import {
  ActionSheetController,
  ModalController,
  AlertController,
} from '@ionic/angular';
import { AdminService } from 'src/app/services/admin.service';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { Router, NavigationExtras } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  users;
  constructor(
    private actionSheetController: ActionSheetController,
    private adminService: AdminService,
    private authService: AuthenticateService,
    private router: Router,
    private modalController: ModalController,
    private uiService: UiService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getUsers();
  }

  displayErrorMessage(message) {
    this.authService.changeErrorMessage(message);
    this.authService.displayErrorPage();
  }

  async authenticate() {
    try {
      const response = await this.authService.verifyToken();
      if (!response) {
        this.uiService.dismissLoading();
        this.displayErrorMessage('Cannot authenticate');
      }
      return response;
    } catch (error) {
      this.uiService.dismissLoading();
      this.displayErrorMessage('Cannot authenticate');
    }
  }

  async getUsers(event = null) {
    await this.uiService.presentLoading();
    const auth = await this.authenticate();
    if (!auth) {
      this.uiService.dismissRefreshContent(event);
      return;
    }
    try {
      const response: any = await this.adminService.getUsers();
      this.users = response.data;
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
      if (!this.users) {
        this.displayErrorMessage('Cannot get users');
      }
    } catch (error) {
      this.uiService.dismissRefreshContent(event);
      this.uiService.dismissLoading();
      if (error.error.display) {
        this.displayErrorMessage(error.error.display);
      }
      this.authService.displayErrorPage();
    }
  }

  openFullInfo(user) {}

  async placeOrder(user) {
    let navigationExtras: NavigationExtras = {
      state: {
        user,
      },
    };

    this.router.navigate([`users/order/${user.userId}`], navigationExtras);
  }

  async handleButtonClick(user) {
    const actionSheet = await this.actionSheetController.create({
      header: user.fullName,
      buttons: [
        {
          text: 'Place Order',
          icon: 'cash',
          handler: () => {
            this.placeOrder(user);
          },
        },
        {
          text: 'Full Information',
          icon: 'information-circle',
          handler: () => {
            this.adminService.setUserData(user);
            this.router.navigateByUrl('users/orders');
          },
        },
        {
          text: 'Edit',
          icon: 'pencil',
          handler: () => {
            let navigationExtras: NavigationExtras = {
              state: {
                user,
              },
            };
            this.router.navigate(['users/post'], navigationExtras);
          },
        },
        {
          text: 'Delete',
          icon: 'trash',
          role: 'destructive',
          handler: async () => {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: `Delete`,
              message: `Are you sure you want to delete  ${user.fullName}?`,
              buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass: 'secondary',
                },
                {
                  text: 'Yes',
                  handler: async () => {
                    try {
                      let response: any = await this.adminService.deleteUser(
                        user.userId
                      );
                      if (response.data) {
                        this.uiService.presentToast('deleted user');
                        this.getUsers();
                        return;
                      }
                      this.uiService.presentToast(
                        'cannot delete db user',
                        'danger'
                      );
                    } catch (error) {
                      this.uiService.presentToast(
                        'cannot delete user',
                        'danger'
                      );
                    }
                  },
                },
              ],
            });

            await alert.present();
          },
        },
      ],
    });

    await actionSheet.present();
  }
}
