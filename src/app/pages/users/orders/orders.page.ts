import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  deliveries;
  user: any;
  show = false;
  constructor(
    private adminService: AdminService,
    private actionSheetController: ActionSheetController,
    private router: Router,
    private alertController: AlertController,
    private uiService: UiService
  ) {}

  ngOnInit() {}

  async ionViewWillEnter() {
    this.user = await this.adminService.data.user;
    this.getOrders();
    this.show = true;
  }

  async getOrders(event = null) {
    try {
      const response: any = await this.adminService.getUserOrders(
        this.user.userId
      );

      this.deliveries = response.data;
      this.uiService.dismissRefreshContent(event);
    } catch (error) {
      const message =
        (error.error && error.error.message) || 'Cannot get orders';
      this.uiService.presentToast(message, 'danger');
      this.uiService.dismissRefreshContent(event);
    }
  }

  async handleButtonClick(delivery) {
    const actionSheet = await this.actionSheetController.create({
      header: delivery.datetime,
      buttons: [
        {
          text: 'Edit',
          icon: 'pencil',
          handler: () => {
            let navigationExtras: NavigationExtras = {
              state: {
                user: {
                  ...this.user,
                  delivery,
                },
              },
            };

            this.router.navigate(
              [`users/order/${this.user.userId}`],
              navigationExtras
            );
          },
        },
        {
          text: 'Delete',
          icon: 'trash',
          role: 'destructive',
          handler: async () => {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: `Delete`,
              message: `Are you sure you want to delete  ${delivery.datetime}?`,
              buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass: 'secondary',
                },
                {
                  text: 'Yes',
                  handler: async () => {
                    try {
                      let response: any = await this.adminService.deleteOrder(
                        delivery.deliveryId
                      );
                      if (response.data) {
                        this.uiService.presentToast('Deleted order');
                        this.getOrders(this.user.userId);
                        return;
                      }
                      this.uiService.presentToast(
                        'cannot delete db order',
                        'danger'
                      );
                    } catch (error) {
                      this.uiService.presentToast(
                        'cannot delete order',
                        'danger'
                      );
                    }
                  },
                },
              ],
            });

            await alert.present();
          },
        },
      ],
    });

    await actionSheet.present();
  }
}
