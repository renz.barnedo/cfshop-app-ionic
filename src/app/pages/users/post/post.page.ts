import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';
import { AdminService } from 'src/app/services/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {
  settings = {
    fields: [
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'First Name',
        formControlName: 'firstName',
        dropdown: [],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'text',
        label: 'Last Name',
        formControlName: 'lastName',
        dropdown: [],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'textarea',
        label: 'Address',
        formControlName: 'address',
        dropdown: [],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'number',
        label: 'Contact Number',
        formControlName: 'cellphone',
        dropdown: [],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100000000000,
      },
      {
        show: true,
        validators: [],
        default: '',
        type: 'number',
        label: 'Telephone Number',
        formControlName: 'telephone',
        dropdown: [],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100000000000,
      },
      {
        show: true,
        validators: ['required'],
        default: 'user',
        type: 'select',
        label: 'Role',
        formControlName: 'role',
        dropdown: ['user', 'admin'],
        position: 'floating',
        placeholder: '',
        class: '',
        min: 1,
        max: 100,
      },
    ],
  };

  userData: any = this.adminService.data.user;
  userForm: FormGroup;
  show;

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private uiService: UiService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        const passedData = this.router.getCurrentNavigation().extras.state.user;

        if (passedData) {
          this.userData = passedData;
        }
      }
    });
  }

  ngOnInit() {
    this.setUserForm();
  }

  ionViewWillLeave() {
    this.adminService.setUserData(null);
  }

  setUserFormControlsWithData() {
    return this.settings.fields
      .map((field) => {
        const property = field.formControlName;

        const defaultValue =
          property === 'role'
            ? this.userData['user'][property]
            : this.userData[property];
        const control = {
          [property]: this.fb.control(defaultValue),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setUserFormControls() {
    return this.settings.fields
      .map((field) => {
        const validators = field.validators.length ? Validators.required : null;
        const control = {
          [field.formControlName]: this.fb.control(field.default, validators),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setUserForm() {
    const UserFormControls: any = this.userData
      ? this.setUserFormControlsWithData()
      : this.setUserFormControls();
    this.show = true;
    this.userForm = this.fb.group(UserFormControls);
  }

  async insertUser(data) {
    try {
      const response = await this.adminService.insertUser(data);
      this.uiService.presentToast('Inserted user');
      if (!response) {
        this.uiService.presentToast('Cannot insert user');
      }
    } catch (error) {
      this.uiService.presentToast('Cannot insert user');
    }
  }

  async updateUser(data) {
    try {
      const response = await this.adminService.updateUser(data);
      this.uiService.presentToast('Updated user');
      if (!response) {
        this.uiService.presentToast('Cannot update user');
      }
    } catch (error) {
      this.uiService.presentToast(
        error.error.message || 'Cannot update user',
        'danger'
      );
    }
  }

  async onSubmit() {
    if (this.userForm.status === 'INVALID') {
      this.uiService.presentToast("Some fields missin'", 'danger');
      return;
    }

    const data = {
      user: {
        // first middle last address cp telephone
        ...this.userForm.value,
      },
      role: {
        roleId: null,
        // email, username, password, role
        role: this.userForm.value.role.toLowerCase(),
      },
    };
    delete data.user.role;
    if (this.userData) {
      data.user.userId = this.userData.userId;
      data.role.roleId = this.userData.user.roleId;
      await this.updateUser(data);
      return;
    }
    await this.insertUser(data);
  }
}
