import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {
  settings: any = {
    fields: [
      {
        show: true,
        validators: ['required'],
        default: 'All',
        type: 'select',
        label: 'Display',
        formControlName: 'display',
        dropdown: [
          'All',
          'Drinks only',
          'Pastas only',
          'Desserts only',
          'Appetizers only',
          'K-Menu only',
        ],
        position: 'floating',
        placeholder: 'category of product',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: window.screen.width < 426 ? true : false,
        validators: ['required'],
        default: '',
        type: 'select',
        label: 'Columns per row',
        formControlName: 'columnCount',
        dropdown: [1, 2, 3],
        position: '',
        placeholder: 'column count',
        class: '',
        min: 1,
        max: 100,
      },
      {
        show: true,
        validators: ['required'],
        default: '',
        type: 'toggle',
        label: 'Dark Mode',
        formControlName: 'theme',
        dropdown: [],
        position: '',
        placeholder: 'theme of ap',
        class: '',
        min: 1,
        max: 100,
      },
    ],
  };

  filterForm: FormGroup;

  constructor(
    private modalController: ModalController,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    if (!localStorage.filter) {
      this.setProductForm();
      this.setLocalSettings();
      return;
    }
    this.setDefaultsFromLocal();
    this.setProductForm();
  }

  setDefaultsFromLocal() {
    JSON.parse(localStorage.filter).forEach((control, index) => {
      this.settings.fields[index].default = control;
    });
  }

  setLocalSettings() {
    const values = Object.values(this.filterForm.value);
    if (values.length === 2) {
      values.push(values[1]);
      values[1] = 1;
    }
    localStorage.filter = JSON.stringify(values);
  }

  setFilterFormControls() {
    return this.settings.fields
      .map((field) => {
        const control = {
          [field.formControlName]: this.fb.control(field.default),
        };
        return field.show ? control : false;
      })
      .filter((field) => field)
      .reduce((result, current) => Object.assign(result, current), {});
  }

  setProductForm() {
    const filterFormControls: any = this.setFilterFormControls();
    this.filterForm = this.fb.group(filterFormControls);
  }

  setFilter() {
    this.setLocalSettings();
    this.userService.change();
    this.router.navigateByUrl('');
    // this.dismissModal();
  }

  dismissModal() {
    this.modalController.dismiss(null, undefined);
  }
}
