import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, NavigationEnd, RoutesRecognized } from '@angular/router';
import { UserService } from './services/user.service';
import { filter, pairwise } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  dark = false;
  _subscription;
  name;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private userService: UserService
  ) {
    // ? prev url checker
    // this.router.events
    //   .pipe(
    //     filter((evt: any) => evt instanceof RoutesRecognized),
    //     pairwise()
    //   )
    //   .subscribe((events: RoutesRecognized[]) => {
    //   });
    this.initializeApp();
    this.name = userService.name;
    this._subscription = this.userService.nameChange.subscribe((value) => {
      this.setThemeModeFilter();
    });
  }

  ngOnInit() {
    this.setThemeModeFilter();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  setThemeModeFilter() {
    if (localStorage.filter) {
      this.dark = JSON.parse(localStorage.filter)[2];
    } else {
      this.dark = true;
      localStorage.filter = JSON.stringify(['All', 2, true]);
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
